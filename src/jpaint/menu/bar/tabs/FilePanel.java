package jpaint.menu.bar.tabs;

import javax.swing.*;
import java.awt.*;

public class FilePanel extends JToolBar {
    protected final JMenuItem newItem = new JMenuItem("新建");
    protected final JMenuItem openItem = new JMenuItem("打开");
    protected final JMenuItem saveItem = new JMenuItem("保存");
    protected final JMenuItem saveAsItem = new JMenuItem("另存为");
    protected final JMenuItem aboutItem = new JMenuItem("关于");
    protected final JMenuItem exitItem = new JMenuItem("退出");

    public FilePanel(String name, int orientation) {
        super(name ,orientation);

        this.add(newItem);
        this.add(openItem);
        this.add(saveItem);
        this.add(saveAsItem);
        this.add(aboutItem);
        this.add(exitItem);

        newItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        openItem.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
        saveItem.setAccelerator(KeyStroke.getKeyStroke("ctrl S"));
        saveAsItem.setAccelerator(KeyStroke.getKeyStroke("ctrl A"));
        aboutItem.setAccelerator(KeyStroke.getKeyStroke("ctrl T"));
        exitItem.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));

        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBackground(new Color(245, 246, 247));
    }

    public JMenuItem getNewItem() {
        return newItem;
    }

    public JMenuItem getOpenItem() {
        return openItem;
    }

    public JMenuItem getSaveItem() {
        return saveItem;
    }

    public JMenuItem getSaveAsItem() {
        return saveAsItem;
    }

    public JMenuItem getAboutItem() {
        return aboutItem;
    }

    public JMenuItem getExitItem() {
        return exitItem;
    }
}
