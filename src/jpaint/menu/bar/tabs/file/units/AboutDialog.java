package jpaint.menu.bar.tabs.file.units;

import jpaint.JPaint;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class AboutDialog extends JDialog {

    final ImageCanvas aboutImage = new ImageCanvas();
    public AboutDialog(JPaint jPaint) {
        super(jPaint, "����", false);
        aboutImage.setPreferredSize(new Dimension(430,255));
        this.add(aboutImage);
        this.setVisible(true);
        this.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        this.setLocation(jPaint.getX() + jPaint.getWidth() / 2 - 430 / 2, jPaint.getY() + jPaint.getHeight() / 2 - 255 / 2);
        this.pack();
    }

    private static class ImageCanvas extends Canvas{
        @Override
        public void paint(Graphics g) {
            try {
                g.drawImage(ImageIO.read(new File("image//about.png")),0,0,430,255,null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
