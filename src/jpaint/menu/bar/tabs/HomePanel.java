package jpaint.menu.bar.tabs;

import jpaint.menu.bar.tabs.home.units.*;
import javax.swing.*;
import java.awt.*;

public class HomePanel extends JToolBar {

    protected final ToolsPanel toolsPanel = new ToolsPanel();
    protected final ShapesPanel shapesPanel = new ShapesPanel();
    protected final StrokePanel strokePanel = new StrokePanel();
    protected final ColorPanel colorPanel = new ColorPanel();
    protected final TextPanel textPanel = new TextPanel();
    public HomePanel(String name, int orientation) {
        super(name, orientation);

        this.add(toolsPanel);
        this.add(shapesPanel);
        this.add(strokePanel);
        this.add(colorPanel);
        this.add(textPanel);

        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBackground(new Color(245, 246, 247));
    }

    public ToolsPanel getToolsPanel() {
        return toolsPanel;
    }

    public ShapesPanel getShapesPanel() {
        return shapesPanel;
    }

    public StrokePanel getStrokePanel() {
        return strokePanel;
    }

    public TextPanel getTextPanel() {
        return textPanel;
    }

    @Override
    public Component add(Component comp){
        addImpl(comp, null, -1);
        addImpl(new JLabel(new ImageIcon("image//separator.png")), null, -1);
        return comp;
    }

    public ColorPanel getColorPanel() {
        return colorPanel;
    }
}
