package jpaint.menu.bar.tabs;

import javax.swing.*;
import java.awt.*;

public class EditPanel extends JToolBar {
    protected final JMenuItem undoItem = new JMenuItem("����");
    protected final JMenuItem redoItem = new JMenuItem("����");
    protected final JMenuItem clearItem = new JMenuItem("���");
    public EditPanel(String name, int orientation) {
        super(name, orientation);

        this.add(undoItem);
        this.add(redoItem);
        this.add(clearItem);

        undoItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
        redoItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Y"));
        clearItem.setAccelerator(KeyStroke.getKeyStroke("DELETE"));


        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        setBackground(new Color(245, 246, 247));
    }

    public JMenuItem getUndoItem() {
        return undoItem;
    }

    public JMenuItem getRedoItem() {
        return redoItem;
    }

    public JMenuItem getClearItem() {
        return clearItem;
    }
}
