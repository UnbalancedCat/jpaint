package jpaint.menu.bar.tabs.home.units;

import javax.swing.*;
import java.awt.*;


public class ToolsPanel extends JPanel  {

    public final Color deselectColor = new Color(245, 246, 247);
    // public Color selectColor = new Color(201, 224, 247);

    protected final JButton pencil = new JButton(new ImageIcon("image\\pencil.png"));
    protected final JButton bucket = new JButton(new ImageIcon("image\\bucket.png"));
    protected final JButton text = new JButton(new ImageIcon("image\\text.png"));
    protected final JButton eraser = new JButton(new ImageIcon("image\\eraser.png"));
    protected final JButton absorber = new JButton(new ImageIcon("image\\absorber.png"));
    protected final JButton zoom = new JButton(new ImageIcon("image\\select.png"));

    public ToolsPanel() {
        Box vBox = Box.createVerticalBox();
        Box hBox1 = Box.createHorizontalBox();
        Box hBox2 = Box.createHorizontalBox();
        JLabel spaceBox1 = new JLabel(" ");
        JLabel spaceBox2 = new JLabel(" ");
        JLabel spaceBox3 = new JLabel(" ");

        spaceBox1.setPreferredSize(new Dimension(1, 8));
        spaceBox2.setPreferredSize(new Dimension(1, 10));
        spaceBox3.setPreferredSize(new Dimension(1, 20));

        hBox1.add(pencil);
        hBox1.add(bucket);
        hBox1.add(text);
        hBox2.add(eraser);
        hBox2.add(absorber);

        hBox2.add(zoom);
        vBox.add(spaceBox1);
        vBox.add(hBox1);
        vBox.add(spaceBox2);
        vBox.add(hBox2);
        vBox.add(spaceBox3);

        pencil.setBorder(BorderFactory.createEmptyBorder());
        bucket.setBorder(BorderFactory.createEmptyBorder());
        text.setBorder(BorderFactory.createEmptyBorder());
        eraser.setBorder(BorderFactory.createEmptyBorder());
        absorber.setBorder(BorderFactory.createEmptyBorder());
        zoom.setBorder(BorderFactory.createEmptyBorder());

        pencil.setBackground(deselectColor);
        bucket.setBackground(deselectColor);
        text.setBackground(deselectColor);
        eraser.setBackground(deselectColor);
        absorber.setBackground(deselectColor);
        zoom.setBackground(deselectColor);



        this.setLayout(new BorderLayout());
        this.add(vBox);

        this.add(new JLabel("����", SwingConstants.CENTER), BorderLayout.SOUTH);
        this.setBackground(new Color(245, 246, 247));
    }

    public JButton getPencil() {
        return pencil;
    }

    public JButton getEraser() {
        return eraser;
    }

    public JButton getText() {
        return text;
    }

    public JButton getZoom() {
        return zoom;
    }

    public JButton getAbsorber(){
        return absorber;
    }

    public JButton getBucket() {
        return bucket;
    }
}
