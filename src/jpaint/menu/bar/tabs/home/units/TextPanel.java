package jpaint.menu.bar.tabs.home.units;

import javax.swing.*;
import java.awt.*;

public class TextPanel extends JPanel {
    protected final JComboBox<String> fontName = new JComboBox<>();
    protected final JComboBox<String> fontSize = new JComboBox<>();
    protected final JCheckBox[] fontStyle = new JCheckBox[2];
    protected final TextField textField = new TextField("请输入文字");

    public TextPanel() {
        Box vBox = Box.createVerticalBox();
        Box hBox = Box.createHorizontalBox();

        JLabel spaceBox1 = new JLabel(" ");
        JLabel spaceBox2 = new JLabel(" ");
        JLabel spaceBox3 = new JLabel(" ");
        JLabel spaceBox4 = new JLabel(" ");
        JLabel spaceBox5 = new JLabel(" ");
        JLabel spaceBox6 = new JLabel(" ");
        spaceBox1.setPreferredSize(new Dimension(1, 5));
        spaceBox2.setPreferredSize(new Dimension(1, 5));
        spaceBox3.setPreferredSize(new Dimension(5, 1));
        spaceBox4.setPreferredSize(new Dimension(5, 1));
        spaceBox5.setPreferredSize(new Dimension(1, 5));
        spaceBox6.setPreferredSize(new Dimension(1, 5));

        fontName.addItem("宋体");
        fontName.addItem("楷体");
        fontName.addItem("黑体");

        fontSize.addItem("自动");
        fontSize.addItem("小初");
        fontSize.addItem("四号");
        fontSize.addItem("五号");
        fontSize.addItem("10");
        fontSize.addItem("36");

        fontStyle[0] = new JCheckBox(new ImageIcon("image\\bold.png"));
        fontStyle[1] = new JCheckBox(new ImageIcon("image\\italic.png"));

        hBox.add(fontSize);
        hBox.add(spaceBox3);
        hBox.add(fontStyle[0]);
        hBox.add(spaceBox4);
        hBox.add(fontStyle[1]);

        vBox.add(spaceBox1);
        vBox.add(fontName);
        vBox.add(spaceBox2);
        vBox.add(hBox);
        vBox.add(spaceBox5);
        vBox.add(textField);
        vBox.add(spaceBox6);

        fontName.setBorder(BorderFactory.createEmptyBorder());
        fontSize.setBorder(BorderFactory.createEmptyBorder());
        fontStyle[0].setBorder(BorderFactory.createEmptyBorder());
        fontStyle[1].setBorder(BorderFactory.createEmptyBorder());

        this.setLayout(new BorderLayout());
        this.add(vBox);
        this.add(new JLabel("文字", SwingConstants.CENTER), BorderLayout.SOUTH);
        this.setBackground(new Color(245, 246, 247));
    }

    public JComboBox<String> getFontName() {
        return fontName;
    }
    public JComboBox<String> getFontSize() {
        return fontSize;
    }
    public JCheckBox[] getFontStyle() {
        return fontStyle;
    }
    public TextField getTextField() {
        return textField;
    }
}
