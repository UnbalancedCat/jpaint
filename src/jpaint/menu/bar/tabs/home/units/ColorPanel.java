package jpaint.menu.bar.tabs.home.units;

import javax.swing.*;
import java.awt.*;

public class ColorPanel extends JPanel {

    public enum selectColor {FORECOLOR, BACKCOLOR}

    protected selectColor currentSelectColor = selectColor.FORECOLOR;
    protected final JButton foreColorButton = new JButton(new ImageIcon("image//colorBase.png"));
    protected final JButton backColorButton = new JButton(new ImageIcon("image//colorBase.png"));
    protected final JButton editColor = new JButton(new ImageIcon("image//editColor.png"));

    public ColorPanel() {
        Box hBox = Box.createHorizontalBox();
        Box vBox1 = Box.createVerticalBox();
        Box vBox2 = Box.createVerticalBox();
        Box vBox4 = Box.createVerticalBox();
        JLabel vSpaceBox1 = new JLabel(" ");
        JLabel vSpaceBox2 = new JLabel(" ");
        JLabel vSpaceBox3 = new JLabel(" ");
        JLabel hSpaceBox1 = new JLabel(" ");
        JLabel hSpaceBox2 = new JLabel(" ");
        JLabel hSpaceBox3 = new JLabel(" ");
        JLabel hSpaceBox4 = new JLabel(" ");
        JLabel hSpaceBox5 = new JLabel(" ");
        JLabel hSpaceBox6 = new JLabel(" ");
        JLabel hSpaceBox7 = new JLabel(" ");
        JLabel hSpaceBox8 = new JLabel(" ");
        JLabel hSpaceBox9 = new JLabel(" ");
        JLabel hSpaceBox10 = new JLabel(" ");
        JLabel hSpaceBox11 = new JLabel(" ");
        JLabel hSpaceBox12 = new JLabel(" ");

        vSpaceBox1.setPreferredSize(new Dimension(12, 1));
        vSpaceBox2.setPreferredSize(new Dimension(12, 1));
        vSpaceBox3.setPreferredSize(new Dimension(12, 1));
        hSpaceBox1.setPreferredSize(new Dimension(1, 3));
        hSpaceBox2.setPreferredSize(new Dimension(1, 8));
        hSpaceBox3.setPreferredSize(new Dimension(1, 12));
        hSpaceBox4.setPreferredSize(new Dimension(1, 7));
        hSpaceBox5.setPreferredSize(new Dimension(1, 12));
        hSpaceBox6.setPreferredSize(new Dimension(1, 12));
        hSpaceBox7.setPreferredSize(new Dimension(1, 7));
        hSpaceBox8.setPreferredSize(new Dimension(1, 12));
        hSpaceBox9.setPreferredSize(new Dimension(1, 12));
        hSpaceBox10.setPreferredSize(new Dimension(1, 7));
        hSpaceBox11.setPreferredSize(new Dimension(1, 5));
        hSpaceBox12.setPreferredSize(new Dimension(1, 10));


        foreColorButton.setBackground(Color.BLACK);
        foreColorButton.setPreferredSize(new Dimension(50, 50));
        backColorButton.setBackground(Color.WHITE);
        backColorButton.setPreferredSize(new Dimension(42, 42));
        editColor.setPreferredSize(new Dimension(36, 36));

        vBox1.add(hSpaceBox1);
        vBox1.add(foreColorButton);
        vBox1.add(hSpaceBox2);
        vBox1.add(new JLabel(" ǰ��ɫ", SwingConstants.CENTER));
        vBox1.add(hSpaceBox3);

        vBox2.add(hSpaceBox4);
        vBox2.add(backColorButton);
        vBox2.add(hSpaceBox5);
        vBox2.add(new JLabel(" ����ɫ", SwingConstants.CENTER));
        vBox2.add(hSpaceBox6);

        vBox4.add(hSpaceBox10);
        vBox4.add(editColor);
        vBox4.add(hSpaceBox11);
        vBox4.add(new JLabel("  �༭", SwingConstants.CENTER));
        vBox4.add(new JLabel("  ��ɫ", SwingConstants.CENTER));
        vBox4.add(hSpaceBox12);

        hBox.add(vBox1);
        hBox.add(vSpaceBox1);
        hBox.add(vBox2);
        hBox.add(vSpaceBox2);
        hBox.add(vBox4);

        this.setLayout(new BorderLayout());
        this.add(hBox);
        this.add(new JLabel("��ɫ", SwingConstants.CENTER), BorderLayout.SOUTH);
        this.setBackground(new Color(245, 246, 247));
    }

    public selectColor getCurrentSelectColor() {
        return currentSelectColor;
    }

    public void setCurrentSelectColor(selectColor currentSelectColor) {
        this.currentSelectColor = currentSelectColor;
    }

    public JButton getEditColor() {
        return editColor;
    }

    public JButton getForeColorButton() {
        return foreColorButton;
    }

    public JButton getBackColorButton() {
        return backColorButton;
    }
}
