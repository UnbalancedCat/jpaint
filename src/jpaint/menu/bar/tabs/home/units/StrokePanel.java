package jpaint.menu.bar.tabs.home.units;

import javax.swing.*;
import java.awt.*;

public class StrokePanel extends JPanel {
    protected final JComboBox<Float> wid = new JComboBox<>();

    public StrokePanel() {
        Box vBox = Box.createVerticalBox();
        Box hBox = Box.createHorizontalBox();
        JLabel spaceBox1 = new JLabel(" ");
        JLabel spaceBox2 = new JLabel(" ");
        JLabel spaceBox3 = new JLabel(" ");

        spaceBox1.setPreferredSize(new Dimension(1, 5));
        spaceBox2.setPreferredSize(new Dimension(1, 14));
        spaceBox3.setPreferredSize(new Dimension(1, 20));

        for (float i = 1.0F; i <= 10.0F; i++) {
            wid.addItem(i);
        }
        wid.setSelectedIndex(2);

        hBox.add(new JLabel(new ImageIcon("image//width.png"), SwingConstants.CENTER));
        vBox.add(spaceBox1);
        vBox.add(hBox);
        vBox.add(spaceBox2);
        vBox.add(wid);
        vBox.add(spaceBox3);

        wid.setBorder(BorderFactory.createEmptyBorder());

        this.setLayout(new BorderLayout());
        this.add(vBox);
        this.add(new JLabel("��ϸ", SwingConstants.CENTER), BorderLayout.SOUTH);
        this.setBackground(new Color(245, 246, 247));
    }
    
    public JComboBox<Float> getWid() {
        return wid;
    }
}