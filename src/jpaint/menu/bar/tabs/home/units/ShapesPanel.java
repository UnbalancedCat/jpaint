package jpaint.menu.bar.tabs.home.units;

import javax.swing.*;
import java.awt.*;

public class ShapesPanel extends JPanel {
    public final Color deselectColor = new Color(238, 238, 238);
    public enum outlineType {NULL, COLORED}
    protected outlineType outlineTypeSelect = outlineType.COLORED;


    protected final JButton line = new JButton(new ImageIcon("image\\line.png"));
    protected final JButton circle = new JButton(new ImageIcon("image\\circle.png"));
    protected final JButton oval = new JButton(new ImageIcon("image\\oval.png"));
    protected final JButton rect = new JButton(new ImageIcon("image\\rect.png"));
    protected final JComboBox<String> outline = new JComboBox<>();
    protected final JComboBox<String> fill = new JComboBox<>();

    public ShapesPanel() {
        Box hBox = Box.createHorizontalBox();
        JPanel shape = new JPanel(new FlowLayout(FlowLayout.LEFT));
        Box vBox = Box.createVerticalBox();
        Box hBox1 = Box.createHorizontalBox();
        Box hBox2 = Box.createHorizontalBox();

        outline.addItem("纯色");
        outline.addItem("无轮廓线");

        fill.addItem("不填充");
        fill.addItem("纯色");

        shape.add(line);
        shape.add(circle);
        shape.add(oval);
        shape.add(rect);
        hBox1.add(new JLabel(new ImageIcon("image//outline.png")));
        hBox1.add(outline);
        hBox2.add(new JLabel(new ImageIcon("image//fill.png")));
        hBox2.add(fill);

        vBox.add(hBox1);
        vBox.add(hBox2);
        vBox.add(Box.createVerticalGlue());
        hBox.add(shape);
        hBox.add(vBox);

        line.setBorder(BorderFactory.createEmptyBorder());
        circle.setBorder(BorderFactory.createEmptyBorder());
        oval.setBorder(BorderFactory.createEmptyBorder());
        rect.setBorder(BorderFactory.createEmptyBorder());
        outline.setBorder(BorderFactory.createEmptyBorder());
        fill.setBorder(BorderFactory.createEmptyBorder());

        line.setBackground(deselectColor);
        circle.setBackground(deselectColor);
        oval.setBackground(deselectColor);
        rect.setBackground(deselectColor);

        shape.setPreferredSize(new Dimension(150, 90));

        this.setLayout(new BorderLayout());
        this.add(hBox);
        this.add(new JLabel("形状", SwingConstants.CENTER), BorderLayout.SOUTH);
        this.setBackground(new Color(245, 246, 247));
    }

    public JButton getLine() {
        return line;
    }

    public JButton getCircle() {
        return circle;
    }

    public JButton getOval() {
        return oval;
    }

    public JButton getRect() {
        return rect;
    }

    public JComboBox<String> getOutline() {
        return outline;
    }

    public JComboBox<String> getFill() {
        return fill;
    }

    public outlineType getOutlineTypeSelect() {
        return outlineTypeSelect;
    }

    public void setOutlineTypeSelect(outlineType outlineTypeSelect) {
        this.outlineTypeSelect = outlineTypeSelect;
    }
}
