package jpaint.menu.bar;

import jpaint.menu.bar.tabs.*;

import javax.swing.*;
import java.awt.*;


public class MenuBar extends JTabbedPane {

    protected final FilePanel filePanel = new FilePanel("文件", SwingConstants.HORIZONTAL);
    protected final EditPanel editPanel = new EditPanel("编辑", SwingConstants.HORIZONTAL);
    protected final HomePanel homePanel = new HomePanel("主页",SwingConstants.HORIZONTAL);

    public MenuBar()
    {
        // First level menu (tab)
        this.addTab("文件", filePanel);
        this.addTab("查看", editPanel);
        this.addTab("主页", homePanel);

        this.setSelectedIndex(2);
    }

    public HomePanel getHomePanel() {
        return homePanel;
    }

    public EditPanel getEditPanel() {
        return editPanel;
    }

    public FilePanel getFilePanel() {
        return filePanel;
    }

    @Override
    public void addTab(String title, Component component) {
        addTab(component.getName(), null, component);
    }



}
