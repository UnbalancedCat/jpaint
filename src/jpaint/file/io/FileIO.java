package jpaint.file.io;

import jpaint.JPaint;
import jpaint.draw.area.DrawArea;
import jpaint.draw.area.DrawArea.CanvasPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class FileIO {
    protected final JFileChooser saveFileChooser = new JFileChooser();
    protected final JFileChooser openFileChooser = new JFileChooser();

    protected final FileNameExtensionFilter jpgFilter = new FileNameExtensionFilter("JPG(*.jpg)", "jpg");
    protected final FileNameExtensionFilter pngFilter = new FileNameExtensionFilter("PNG(*.png)", "png");
    protected final FileNameExtensionFilter jpaintFilter = new FileNameExtensionFilter("JPAINT(*.jpaint)", "jpaint");

    public FileIO() {
        saveFileChooser.setFileFilter(jpgFilter);
        saveFileChooser.setFileFilter(pngFilter);
        saveFileChooser.setFileFilter(jpaintFilter);
        openFileChooser.setFileFilter(jpgFilter);
        openFileChooser.setFileFilter(pngFilter);
        openFileChooser.setFileFilter(jpaintFilter);
    }

    public File saveAsFile(JPaint jPaint, DrawArea drawArea) {
        CanvasPanel canvasPanel = drawArea.getCanvas();

        saveFileChooser.setSelectedFile(new File(jPaint.getFileName()));
        if(saveFileChooser.showSaveDialog(jPaint) == JFileChooser.CANCEL_OPTION) {
            return null;
        }
        else{
            File file = saveFileChooser.getSelectedFile();
            String fileType = "jpaint";
            if(saveFileChooser.getFileFilter() == jpgFilter) {
                fileType = "jpg";
            }
            else if(saveFileChooser.getFileFilter() == pngFilter) {
                fileType = "png";
            }
            else if(saveFileChooser.getFileFilter() == jpaintFilter){
                fileType = "jpaint";
            }
            if(file != null){
                file = new File(file.getAbsoluteFile() + "." + fileType);

                save(jPaint, drawArea, canvasPanel, fileType, file);

                String newFileName = saveFileChooser.getSelectedFile().toString().replace(saveFileChooser.getCurrentDirectory().toString() + "\\", "");
                jPaint.setFilePath(saveFileChooser.getSelectedFile().toString());
                jPaint.setFileType(fileType);
                jPaint.setFileName(newFileName);
                jPaint.setTitle(newFileName + " - 画图");
            }

            return file;
        }
    }

    public File saveFile(JPaint jPaint, DrawArea drawArea) {
        CanvasPanel canvasPanel = drawArea.getCanvas();

        if(jPaint.getFilePath() != null){
            String fileType = jPaint.getFileType();
            File file = new File(jPaint.getFilePath() + "." + fileType);

            save(jPaint, drawArea, canvasPanel, fileType, file);
            return file;
        }
        else {
            return saveAsFile(jPaint, drawArea);
        }
    }

    private void save(JPaint jPaint, DrawArea drawArea, CanvasPanel canvasPanel, String fileType, File file) {
        if(fileType.equals("jpaint")){
            try (FileOutputStream out = new FileOutputStream(file)) {
                OutputStreamWriter outWriter = new OutputStreamWriter(out, "GBK");
                outWriter.append(drawArea.getInfo());
                outWriter.close();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(jPaint, "保存出错！请重试！");
                e.printStackTrace();
            }
        }
        else {
            try {
                ImageIO.write(canvasPanel.getImage(), fileType, file);
            } catch (IOException e) {
                JOptionPane.showMessageDialog(jPaint, "保存出错！请重试！");
                e.printStackTrace();
            }
        }
    }

    public void openFile(JPaint jPaint, DrawArea drawArea) {
        if(openFileChooser.showOpenDialog(jPaint) == JFileChooser.CANCEL_OPTION) {
            return;
        }
        String[] fileFullName = openFileChooser.getSelectedFile().toString().replace(openFileChooser.getCurrentDirectory().toString() + "\\", "").split("\\.");
        String fileType = fileFullName[fileFullName.length - 1];
        int width = drawArea.getPreferredSize().width;
        int height = drawArea.getPreferredSize().height;

        if(fileType.equals("jpaint")) {
            try (FileInputStream in = new FileInputStream(openFileChooser.getSelectedFile().toString())) {
                InputStreamReader inReader = new InputStreamReader(in, "GBK");
                int len;
                StringBuilder buffer = new StringBuilder();
                while((len = inReader.read()) != -1) {
                    buffer.append((char)len);
                }
                inReader.close();
                String[] info = buffer.toString().split("\n");
                drawArea.setInfo(info);
                width = Integer.parseInt(info[0].replace("canvasWidth:", ""));
                height = Integer.parseInt(info[1].replace("canvasHeight:", ""));
            } catch (Exception e) {
                JOptionPane.showMessageDialog(jPaint, "打开出错！请重试！");
                e.printStackTrace();
                newFile(jPaint, drawArea);
                return;
            }
        }
        else if(fileType.equals("png") || fileType.equals("jpg")) {
            File file = openFileChooser.getSelectedFile();
            try {
                BufferedImage image = ImageIO.read(new FileInputStream(file));
                drawArea.setInfo(image);
                width = image.getWidth();
                height = image.getHeight();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(jPaint, "打开出错！请重试！");
                e.printStackTrace();
                newFile(jPaint, drawArea);
                return;
            }
        }
        drawArea.getCanvas().setPreferredSize(new Dimension(width, height));
        drawArea.setPreferredSize(new Dimension(width, height));
        drawArea.getCanvas().setSize(width, height);
        drawArea.setSize(width, height);

        StringBuilder newFileName = new StringBuilder();
        for(int i = 0; i < fileFullName.length - 1; i++) {
            newFileName.append(fileFullName[i]);
        }
        jPaint.setFilePath(openFileChooser.getSelectedFile().toString().replace("." + fileType, ""));
        jPaint.setFileType(fileType);
        jPaint.setFileName(newFileName.toString());
        jPaint.setTitle(newFileName + " - 画图");
    }

    public void newFile(JPaint jPaint, DrawArea drawArea) {
        int width;
        int height;
        width = 800;
        height = 450;
        drawArea.getCanvas().setPreferredSize(new Dimension(width, height));
        drawArea.setPreferredSize(new Dimension(width, height));
        drawArea.getCanvas().setSize(width, height);
        drawArea.setSize(width, height);

        drawArea.setInfo();

        jPaint.setFilePath(null);
        jPaint.setFileType(null);
        jPaint.setFileName("无标题");
        jPaint.setTitle("无标题 - 画图");
        jPaint.repaint();
    }
}
