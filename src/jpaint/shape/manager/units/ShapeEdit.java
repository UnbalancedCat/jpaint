package jpaint.shape.manager.units;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.Objects;

import jpaint.draw.area.DrawArea;
import jpaint.draw.area.shapes.Shape;
import jpaint.draw.area.shapes.Text;
import jpaint.shape.manager.ShapeManager;

public class ShapeEdit extends JFrame implements Serializable {
    protected final TextField nameField = new TextField("");
    protected final JComboBox<String> outline = new JComboBox<>();
    protected final JComboBox<String> fill = new JComboBox<>();
    protected final JComboBox<Float> wid = new JComboBox<>();
    public enum selectColor {FORECOLOR, BACKCOLOR}
    protected selectColor currentSelectColor = selectColor.FORECOLOR;
    protected final JButton foreColorButton = new JButton(new ImageIcon("image//colorBase.png"));
    protected final JButton backColorButton = new JButton(new ImageIcon("image//colorBase.png"));
    protected final JButton editColor = new JButton(new ImageIcon("image//editColor.png"));
    protected Color foreColor;
    protected Color backColor;

    protected final JComboBox<String> fontName = new JComboBox<>();
    protected final JComboBox<String> fontSize = new JComboBox<>();
    protected final JCheckBox[] fontStyle = new JCheckBox[2];
    protected final TextField textField = new TextField("请输入文字");

    protected final JButton deleteButton = new JButton("删除形状");
    protected final JButton submitButton = new JButton("确定修改");

    public ShapeEdit(DrawArea drawArea, ShapeManager shapeManager, Shape shape) {
        Box vBox = Box.createVerticalBox();
        Box hBox1 = Box.createHorizontalBox();
        Box hBox2 = Box.createHorizontalBox();
        Box hBox5 = Box.createHorizontalBox();

        nameField.setText(shape.getName());

        outline.addItem("纯色");
        outline.addItem("无轮廓线");
        fill.addItem("不填充");
        fill.addItem("纯色");
        for (float i = 1.0F; i <= 10.0F; i++) {
            wid.addItem(i);
            if (shape.getStroke() == i) {
                wid.setSelectedItem(i);
            }
        }
        if (shape.getColor().getAlpha() == 0) {
            outline.setSelectedIndex(1);
        }
        if (shape.isFilled()) {
            fill.setSelectedIndex(1);
        }

        foreColorButton.setBackground(shape.getColor());
        foreColorButton.setPreferredSize(new Dimension(50, 50));
        backColorButton.setBackground(shape.getBackColor());
        backColorButton.setPreferredSize(new Dimension(42, 42));
        editColor.setPreferredSize(new Dimension(36, 36));
        foreColor = shape.getColor();
        backColor = shape.getBackColor();

        hBox1.add(outline);
        hBox1.add(fill);
        hBox1.add(wid);
        hBox2.add(foreColorButton);
        hBox2.add(backColorButton);
        hBox2.add(editColor);
        vBox.add(nameField);
        vBox.add(hBox1);
        vBox.add(hBox2);
        vBox.add(hBox5);

        if (shape.getType() == 4) {
            Text textShape = (Text) shape;
            Box hBox3 = Box.createHorizontalBox();
            Box hBox4 = Box.createHorizontalBox();

            fontName.addItem("宋体");
            fontName.addItem("楷体");
            fontName.addItem("黑体");
            int index = switch (textShape.getFont().getFontName()) {
                case "楷体" -> 1;
                case "黑体" -> 2;
                default -> 0;
            };
            fontName.setSelectedIndex(index);

            fontSize.addItem("小初");
            fontSize.addItem("四号");
            fontSize.addItem("五号");
            fontSize.addItem("10");
            fontSize.addItem("36");
            fontSize.addItem("自动");
            switch (textShape.getFont().getSize()) {
                case 48 -> index = 0;
                case 18 -> index = 1;
                case 14 -> index = 2;
                case 10 -> index = 3;
                case 36 -> index = 4;
                default -> index = 5;
            }
            fontSize.setSelectedIndex(index);

            fontStyle[0] = new JCheckBox(new ImageIcon("image\\bold.png"));
            fontStyle[1] = new JCheckBox(new ImageIcon("image\\italic.png"));
            switch (textShape.getFont().getStyle()) {
                case Font.BOLD -> {
                    fontStyle[0].setSelected(true);
                    fontStyle[0].setIcon(new ImageIcon("image\\boldSelected.png"));
                }
                case Font.ITALIC -> {
                    fontStyle[1].setSelected(true);
                    fontStyle[1].setIcon(new ImageIcon("image\\italicSelected.png"));
                }
                case Font.BOLD + Font.ITALIC -> {
                    fontStyle[0].setSelected(true);
                    fontStyle[1].setSelected(true);
                    fontStyle[0].setIcon(new ImageIcon("image\\boldSelected.png"));
                    fontStyle[1].setIcon(new ImageIcon("image\\italicSelected.png"));
                }
            }
            fontStyle[0].addItemListener(e -> {
                JCheckBox cbBold = (JCheckBox) e.getItem();
                if (cbBold.isSelected()) {
                    cbBold.setIcon(new ImageIcon("image\\boldSelected.png"));
                } else {
                    cbBold.setIcon(new ImageIcon("image\\bold.png"));
                }
            });
            fontStyle[1].addItemListener(e -> {
                JCheckBox cbItalic = (JCheckBox) e.getItem();
                if (cbItalic.isSelected()) {
                    cbItalic.setIcon(new ImageIcon("image\\italicSelected.png"));
                } else {
                    cbItalic.setIcon(new ImageIcon("image\\italic.png"));
                }
            });

            textField.setText(textShape.getText());

            hBox3.add(fontName);
            hBox3.add(fontSize);
            hBox3.add(fontStyle[0]);
            hBox3.add(fontStyle[1]);
            hBox4.add(textField);
            vBox.add(hBox3);
            vBox.add(hBox4);
        }

        hBox5.add(deleteButton);
        hBox5.add(submitButton);
        vBox.add(hBox5);

        this.setTitle("编辑形状");
        this.setLayout(new BorderLayout());
        this.add(vBox, BorderLayout.CENTER);
        this.setLocation(650, 350);
        this.pack();
        this.setVisible(true);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("image//JPaint.png"));

        foreColorButton.addActionListener(e -> currentSelectColor = selectColor.FORECOLOR);
        backColorButton.addActionListener(e -> currentSelectColor = selectColor.BACKCOLOR);
        editColor.addActionListener(e -> {
            Color result = JColorChooser.showDialog(this, "编辑颜色", Color.BLACK);
            if (result == null)
                return;
            if(currentSelectColor == selectColor.FORECOLOR) {
                foreColorButton.setBackground(result);
                if (outline.getSelectedIndex() == 0) {
                    foreColor = result;
                } else if(outline.getSelectedIndex() == 1) {
                    foreColor = new Color(result.getRed(), result.getGreen(), result.getBlue(), 0);
                }
            } else if (currentSelectColor == selectColor.BACKCOLOR) {
                backColorButton.setBackground(result);
                backColor = result;
            }
        });

        deleteButton.addActionListener(e -> {
            int selection = JOptionPane.showConfirmDialog(null, "确认删除形状？");
            if (selection == 0) {
                drawArea.getCanvas().getUndoStack().push(new DrawArea.opsRecord(DrawArea.recordType.DELETE, shape.getId(), shape.getRecord()));
                shape.setName("");
            }
            drawArea.setSave(false);
            drawArea.getCanvas().getRedoStack().clear();
            drawArea.getCanvas().repaint();
            shapeManager.setShapes();
            this.setVisible(false);
        });

        submitButton.addActionListener(e -> {
            drawArea.getCanvas().getUndoStack().push(new DrawArea.opsRecord(DrawArea.recordType.UPDATE, shape.getId(), shape.getRecord()));
            if (Objects.equals(nameField.getText(), "")) {
                JOptionPane.showMessageDialog(null, "请输入形状名");
                return;
            } else {
                shape.setName(nameField.getText());
            }
            if (outline.getSelectedIndex() == 1) {
                foreColor = new Color(foreColor.getRed(), foreColor.getGreen(), foreColor.getBlue(), 0);
            }
            shape.setColor(foreColor);
            shape.setBackColor(backColor);
            shape.setStroke((Float) Objects.requireNonNull(wid.getSelectedItem()));
            shape.setFilled(fill.getSelectedIndex() == 1);
            if (shape.getType() == 4) {
                Text textShape = (Text) shape;
                textShape.setText(textField.getText());
                int textLength = 0;
                for(int i = 0; i < textField.getText().length(); i++) {
                    int ascii = Character.codePointAt(textField.getText(), i);
                    if (ascii >= 0 && ascii <= 255)
                        textLength++;
                    else
                        textLength += 2;
                }
                textShape.setTextLength(textLength);
                int size = switch ((String) Objects.requireNonNull(fontSize.getSelectedItem())) {
                    case "小初" -> 48;
                    case "四号" -> 18;
                    case "五号" -> 14;
                    case "10" -> 10;
                    case "36" -> 36;
                    default -> 0;
                };
                int style = (fontStyle[0].isSelected() ? Font.BOLD : 0) + (fontStyle[1].isSelected() ? Font.ITALIC : 0);
                textShape.setFont(new Font((String) fontName.getSelectedItem(), style, size));
            }
            drawArea.setSave(false);
            drawArea.getCanvas().getRedoStack().clear();
            drawArea.getCanvas().repaint();
            shapeManager.setShapes();
            this.setVisible(false);
        });
    }
}
