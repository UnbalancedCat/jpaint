package jpaint.shape.manager.units;

import jpaint.draw.area.shapes.Shape;
import javax.swing.*;
import java.awt.*;
import java.util.Objects;
import java.util.Vector;

public class ListPanel extends JToolBar {
    Vector<Shape> shapeList = new Vector<>();
    final Box vBox = Box.createVerticalBox();

    public ListPanel() {
        this.setLayout(new BorderLayout());
        this.add(vBox, BorderLayout.CENTER);
        this.setFloatable(false);
    }

    public void setShapes() {
        vBox.removeAll();
        for (Shape shape : shapeList) {
            if (!Objects.equals(shape.getName(), "")) {
                JLabel label = new JLabel(shape.getName());
                label.setFont(new Font("", Font.BOLD, 18));
                vBox.add(label);
            }
        }
    }

    public void setShapes(Vector<Shape> shapeList) {
        this.shapeList = shapeList;
        this.setShapes();
    }

    public void addShape(Shape shape) {
        shapeList.add(shape);
    }

}