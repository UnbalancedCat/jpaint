package jpaint.shape.manager;

import jpaint.draw.area.DrawArea;
import jpaint.draw.area.shapes.Shape;
import jpaint.shape.manager.units.ListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class ShapeManager extends JPanel {
    protected final TextField nameField = new TextField("");
    protected final JButton searchButton = new JButton(new ImageIcon("image\\zoom.png"));
    protected final JButton editButton = new JButton(new ImageIcon("image\\edit.png"));
    protected final JTabbedPane tabbedPane = new JTabbedPane();

    public ShapeManager() {
        tabbedPane.addTab("直线", new ImageIcon("image\\line.png"), new JScrollPane(new ListPanel()));
        tabbedPane.addTab("圆形", new ImageIcon("image\\circle.png"), new JScrollPane(new ListPanel()));
        tabbedPane.addTab("椭圆形", new ImageIcon("image\\oval.png"), new JScrollPane(new ListPanel()));
        tabbedPane.addTab("矩形", new ImageIcon("image\\rect.png"), new JScrollPane(new ListPanel()));
        tabbedPane.addTab("文字", new ImageIcon("image\\text.png"), new JScrollPane(new ListPanel()));
        tabbedPane.setTabPlacement(JTabbedPane.LEFT);

        searchButton.setBorder(BorderFactory.createEmptyBorder());
        editButton.setBorder(BorderFactory.createEmptyBorder());

        Box hBox = Box.createHorizontalBox();
        hBox.add(nameField);
        hBox.add(searchButton);
        hBox.add(editButton);
        this.setLayout(new BorderLayout());
        this.add(hBox, BorderLayout.NORTH);
        this.add(tabbedPane, BorderLayout.CENTER);
        this.setPreferredSize(new Dimension(200, 50));
    }

    public void setShapes() {
        for (int i = 0; i < tabbedPane.getTabCount(); i++) {
            ListPanel listPanel = (ListPanel) ((JScrollPane) tabbedPane.getComponentAt(i)).getViewport().getComponent(0);
            listPanel.setShapes();
        }
        refreshTab();
    }

    public void setShapes(Vector<Shape> shapeList) {
        if (tabbedPane.getTabCount() > 5) {
            tabbedPane.removeTabAt(5);
        }
        Vector<Vector<Shape>> shapeListArray = new Vector<>(5);
        for (int i = 0; i < 5; i++) {
            shapeListArray.add(new Vector<>());
        }
        for (int i = 1; i < shapeList.size(); i++) {
            if(shapeList.get(i).getType() >= DrawArea.selectType.LINE.ordinal() && shapeList.get(i).getType() <= DrawArea.selectType.TEXT.ordinal()) {
                shapeListArray.get(shapeList.get(i).getType()).add(shapeList.get(i));
            }
        }
        for (int i = 0; i < 5; i++) {
            ListPanel listPanel = (ListPanel) ((JScrollPane) tabbedPane.getComponentAt(i)).getViewport().getComponent(0);
            listPanel.setShapes(shapeListArray.get(i));
        }
    }

    public void addShapeTab(String name, Vector<Shape> shapeList) {
        if (tabbedPane.getTabCount() == 6) {
            tabbedPane.removeTabAt(tabbedPane.getTabCount() - 1);
        }
        if (name.length() > 5) {
            name = name.substring(0, 5) +"...";
        }
        tabbedPane.addTab(name, new ImageIcon("image\\zoom.png"), new JScrollPane(new ListPanel()));
        ListPanel listPanel = (ListPanel) ((JScrollPane) tabbedPane.getComponentAt(tabbedPane.getTabCount() - 1)).getViewport().getComponent(0);
        listPanel.setShapes(shapeList);
        tabbedPane.setSelectedIndex(5);
    }

    public void addShape(Shape shape) {
        ListPanel listPanel = (ListPanel) ((JScrollPane) tabbedPane.getComponentAt(shape.getType())).getViewport().getComponent(0);
        listPanel.addShape(shape);
        tabbedPane.setSelectedIndex(shape.getType());
        refreshTab();
    }

    private void refreshTab() {
        if (tabbedPane.getSelectedIndex() > 0) {
            int index = tabbedPane.getSelectedIndex();
            tabbedPane.setSelectedIndex(0);
            tabbedPane.setSelectedIndex(index);
        } else {
            tabbedPane.setSelectedIndex(1);
            tabbedPane.setSelectedIndex(0);
        }
    }

    public TextField getNameField() {
        return nameField;
    }

    public JButton getSearchButton() {
        return searchButton;
    }

    public JButton getEditButton() {
        return editButton;
    }
}
