package jpaint.draw.area.brushes;

import jpaint.draw.area.shapes.Line;
import jpaint.draw.area.shapes.Shape;

import java.awt.*;
import java.util.ArrayList;

public class Pencil extends Shape {
    private final ArrayList<Line> lineSet = new ArrayList<>();

    public Pencil(int id, String name, int type, int xPos, int yPos, Color color, float stroke) {
        super(id, name, type, xPos, yPos, color, stroke, false);
    }

    public Pencil(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke) {
        super(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, false);
    }

    public void add(Line line) {
        lineSet.add(line);
    }

    @Override
    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        g2.setColor(color);
        g2.setStroke(new BasicStroke(stroke));
        for(Line line : lineSet) {
            line.drawShape(g2, false);
        }
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        return false;
    }

    public static class PencilRecord extends ShapeRecord {
        public PencilRecord(String shapeName, int shapeType, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke, boolean isFilled) {
            super(shapeName, shapeType, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
        }
    }

    @Override
    public ShapeRecord getRecord() {
        return new PencilRecord(name, 4, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
    }

    @Override
    public void setBackColor(Color color) {

    }

    @Override
    public Color getBackColor() {
        return null;
    }

    public String getInfo() {
        if(super.getInfo().equals("")) {
            return "";
        }
        else {
            StringBuilder buffer = new StringBuilder(super.getInfo() + " ");
            for(Line line: lineSet) {
                buffer.append("(").append(line.getShortInfo()).append(") ");
            }
            buffer.deleteCharAt(buffer.length() - 1);
            return buffer.toString();
        }
    }
}
