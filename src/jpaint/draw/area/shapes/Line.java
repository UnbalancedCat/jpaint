package jpaint.draw.area.shapes;

import java.awt.*;

public class Line extends Shape {
    public Line(int id, String name, int type, int xPosStart, int yPosStart, Color color, float stroke) {
        super(id, name, type, xPosStart, yPosStart, color, stroke, false);
    }

    public Line(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd,  Color color, float stroke) {
        super(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, false);
    }

    public Line(int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke) {
        super(xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke);
    }

    @Override
    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        super.highlight(g2, isHighlighted);
        g2.setColor(color);
        g2.setStroke(new BasicStroke(stroke));
        g2.drawLine(xPosStart, yPosStart, xPosEnd, yPosEnd);
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        int width = (int) Math.max(4, this.stroke / 4);
        float k = (float)(yPosEnd - yPosStart) / (xPosEnd - xPosStart);
        float x = Math.min(xPosStart, xPosEnd);
        float y = (x == xPosStart) ? yPosStart : yPosEnd;
        for ( ; x <= Math.max(xPosStart, xPosEnd); x++) {
            y = y + k;
            if (xPos <= x + width && xPos >= x - width && yPos <= y + width && yPos >= y - width) {
                return true;
            }
        }
        return false;
    }

    public static class LineRecord extends ShapeRecord {
        public LineRecord(String shapeName, int shapeType, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke, boolean isFilled) {
            super(shapeName, shapeType, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
        }
    }

    @Override
    public ShapeRecord getRecord() {
        return new LineRecord(name, 4, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
    }

    @Override
    public void setBackColor(Color color) {

    }

    @Override
    public Color getBackColor() {
        return color;
    }

    public String getShortInfo() {
        return xPosStart + " " + yPosStart + " " + xPosEnd + " " + yPosEnd;
    }
}
