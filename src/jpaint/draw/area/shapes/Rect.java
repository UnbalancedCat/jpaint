package jpaint.draw.area.shapes;

import java.awt.*;

public class Rect extends Shape {
    protected Color backColor;
    public Rect(int id, String name, int type, int xPoxStart, int yPoxStart, int xPoxEnd, int yPoxEnd, Color color, Color backColor, float stroke, boolean isFilled) {
        super(id, name, type, xPoxStart, yPoxStart, xPoxEnd, yPoxEnd, color, stroke, isFilled);
        this.backColor = backColor;
    }
    public Rect(int id, String name, int type, int xPox, int yPox, Color foreColor, Color backColor, float stroke, boolean isFilled) {
        super(id, name, type, xPox, yPox, foreColor, stroke, isFilled);
        this.backColor = backColor;
    }

    @Override
    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        if (isHighlighted) {
            g2.setColor(Color.BLACK);
            BasicStroke stroke1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, new float[] { 10, 10 }, 0);
            g2.setStroke(stroke1);
            g2.drawRect((int) (Math.min(xPosStart, xPosEnd) - stroke), (int) (Math.min(yPosStart, yPosEnd) - stroke), (int) (Math.abs(xPosStart - xPosEnd) + stroke * 2), (int) (Math.abs(yPosStart - yPosEnd) + stroke * 2));        }
        g2.setStroke(new BasicStroke(stroke));
        if (isFilled) {
            g2.setColor(backColor);
            g2.fillRect(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), (Math.abs(xPosStart - xPosEnd)), (Math.abs(yPosStart - yPosEnd)));
        }
        g2.setColor(color);
        g2.drawRect(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), Math.abs(xPosStart - xPosEnd), Math.abs(yPosStart - yPosEnd));
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        if (this.id == 0) {
            return false;
        }
        return xPos <= Math.max(xPosStart, xPosEnd) && xPos >= Math.min(xPosStart, xPosEnd) && yPos <= Math.max(yPosStart, yPosEnd) && yPos >= Math.min(yPosStart, yPosEnd);
    }

    public static class RectRecord extends ShapeRecord {
        protected final Color backColor;
        public RectRecord(String shapeName, int shapeType, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, Color backcolor, float stroke, boolean isFilled) {
            super(shapeName, shapeType, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
            this.backColor = backcolor;
        }
    }

    @Override
    public ShapeRecord getRecord() {
        return new RectRecord(name, 6, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backColor, stroke, isFilled);
    }

    @Override
    public <T extends ShapeRecord> void recover(T e) {
        super.recover(e);
        if(e instanceof RectRecord record) {
            this.backColor = record.backColor;
        }
    }

    @Override
    public void setBackColor(Color color) {
        this.backColor = color;
    }

    public Color getBackColor() {
        return backColor;
    }

    public String getInfo() {
        if(super.getInfo().equals("")) {
            return "";
        }
        else {
            return super.getInfo() + " " +
                    "backColor:" + backColor.toString();
        }
    }
}
