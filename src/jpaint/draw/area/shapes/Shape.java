package jpaint.draw.area.shapes;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public abstract class Shape extends JLabel implements Serializable {
    protected int id;
    protected String name;
    protected int type;
    protected int xPosStart;
    protected int yPosStart;
    protected int xPosEnd;
    protected int yPosEnd;
    protected int xPosCurrent = -1;
    protected int yPosCurrent = -1;
    protected int xPosMouse = -1;
    protected int yPosMouse = -1;

    protected Color color = new Color(0, 0, 0, 0);
    protected float stroke = 0;
    protected boolean isFilled = false;

    protected Shape(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke, boolean isFilled) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.xPosStart = xPosStart;
        this.yPosStart = yPosStart;
        this.xPosEnd = xPosEnd;
        this.yPosEnd = yPosEnd;

        this.color = color;
        this.stroke = stroke;
        this.isFilled = isFilled;
    }
    protected Shape(int id, String name, int type, int xPos, int yPos, Color color, float stroke, boolean isFilled) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.xPosStart = this.xPosEnd = this.xPosCurrent = xPos;
        this.yPosStart = this.yPosEnd = this.yPosCurrent = yPos;

        this.color = color;
        this.stroke = stroke;
        this.isFilled = isFilled;
    }
    protected Shape(int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke) {
        this.xPosStart = xPosStart;
        this.yPosStart = yPosStart;
        this.xPosEnd = xPosEnd;
        this.yPosEnd = yPosEnd;

        this.color = color;
        this.stroke = stroke;
        this.isFilled = false;
    }
    protected Shape(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd) {
        this.id = id;
        this.name = name;
        this.type = type;

        this.xPosStart = xPosStart;
        this.yPosStart = yPosStart;
        this.xPosEnd = xPosEnd;
        this.yPosEnd = yPosEnd;
    }

    public abstract void drawShape(Graphics2D g2, boolean isHighlighted);
    public abstract boolean isInShape(int xPos, int yPos);
    public abstract ShapeRecord getRecord();
    public <T extends ShapeRecord> void recover(T e){
        this.name = e.shapeName;
        this.xPosStart = e.xPosStart;
        this.xPosEnd = e.xPosEnd;
        this.yPosStart = e.yPosStart;
        this.yPosEnd = e.yPosEnd;
        this.color = e.color;
        this.stroke = e.stroke;
        this.isFilled = e.isFilled;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setPosCurrent(int xPos, int yPos) {
        this.xPosEnd = this.xPosCurrent = xPos;
        this.yPosEnd = this.yPosCurrent = yPos;
    }
    public void setPosMouse(int xPos, int yPos) {
        this.xPosMouse = xPos;
        this.yPosMouse = yPos;
    }
    public void setPosChange(int xPos, int yPos) {
        this.xPosStart = xPosStart - (xPosMouse - xPos);
        this.yPosStart = yPosStart - (yPosMouse - yPos);
        this.xPosEnd = xPosEnd - (xPosMouse - xPos);
        this.yPosEnd = yPosEnd - (yPosMouse - yPos);
        this.xPosMouse = xPos;
        this.yPosMouse = yPos;
    }
    public void setColor(Color color) {
        this.color = color;
    }
    public abstract void setBackColor(Color color);
    public void setStroke(float stroke) {
        this.stroke = stroke;
    }
    public void setFilled(boolean isFilled) {
        this.isFilled = isFilled;
    }

    public int getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public int getType() {
        return this.type;
    }
    public int getXPosCurrent() {
        return xPosCurrent;
    }
    public int getYPosCurrent() {
        return yPosCurrent;
    }
    public Color getColor() {
        return color;
    }
    public abstract Color getBackColor();
    public float getStroke() {
        return stroke;
    }
    public boolean isFilled() {
        return isFilled;
    }
    public String getInfo() {
        if(name.equals("")){
            return "";
        }
        else{
            return "id:" + id + " " +
                    "name:" + name + " " +
                    "type:" + type + " " +
                    "xPosStart:" + xPosStart + " " +
                    "yPosStart:" + yPosStart + " " +
                    "xPosEnd:" + xPosEnd + " " +
                    "yPosEnd:" + yPosEnd + " " +
                    "color:" + color + " " +
                    "stroke:" + stroke + " " +
                    "isFilled:" + isFilled;
        }
    }

    protected void highlight(Graphics2D g2, boolean isHighlighted) {
        if (isHighlighted) {
            g2.setColor(Color.BLACK);
            BasicStroke stroke1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, new float[] { 10, 10 }, 0);
            g2.setStroke(stroke1);
            g2.drawRect(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), Math.abs(xPosStart - xPosEnd), Math.abs(yPosStart - yPosEnd));
        }
    }

    public abstract static class ShapeRecord {
        final public String shapeName;
        @SuppressWarnings("unused")
        final public int shapeType;
        final public int xPosStart;
        final public int yPosStart;
        final public int xPosEnd;
        final public int yPosEnd;
        final public Color color;
        final public float stroke;
        final public boolean isFilled;

        protected ShapeRecord(String shapeName, int shapeType, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, float stroke, boolean isFilled) {
            this.shapeName = shapeName;
            this.shapeType = shapeType;
            this.xPosStart = xPosStart;
            this.yPosStart = yPosStart;
            this.xPosEnd = xPosEnd;
            this.yPosEnd = yPosEnd;
            this.color = color;
            this.stroke = stroke;
            this.isFilled = isFilled;
        }
    }
}