package jpaint.draw.area.shapes;

import java.awt.*;

public class Oval extends Shape {
    protected Color backColor;
    public Oval(int id, String name, int type, int xPos, int yPos, Color color, Color backcolor, float stroke, boolean isFilled) {
        super(id, name, type, xPos, yPos, color, stroke, isFilled);
        this.backColor = backcolor;
    }

    public Oval(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, Color backcolor, float stroke, boolean isFilled) {
        super(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
        this.backColor = backcolor;
    }

    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        super.highlight(g2, isHighlighted);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(stroke));
        if(isFilled) {
            g2.setColor(backColor);
            g2.fillOval(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), Math.abs(xPosStart - xPosEnd), Math.abs(yPosStart - yPosEnd));
        }
        g2.setColor(color);
        g2.drawOval(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), Math.abs(xPosStart - xPosEnd), Math.abs(yPosStart - yPosEnd));
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        int a = Math.abs(xPosStart - xPosEnd) / 2;
        int b = Math.abs(yPosStart - yPosEnd) / 2;
        int centerPointX = (xPosStart + xPosEnd) / 2;
        int centerPointY = (yPosStart + yPosEnd) / 2;
        double v = Math.pow(centerPointX - xPos, 2) / Math.pow(a, 2) + Math.pow(centerPointY - yPos, 2) / Math.pow(b, 2);
        return v < 1;
    }

    public static class OvalRecord extends ShapeRecord {
        protected final Color backColor;
        public OvalRecord(String shapeName, int shapeType, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, Color backcolor, float stroke, boolean isFilled) {
            super(shapeName, shapeType, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
            this.backColor = backcolor;
        }
    }

    @Override
    public ShapeRecord getRecord() {
        return new OvalRecord(name, 4, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backColor, stroke, isFilled);
    }

    public <T extends ShapeRecord> void recover(T e) {
        super.recover(e);
        if(e instanceof OvalRecord record) {
            this.backColor = record.backColor;
        }
    }

    @Override
    public void setBackColor(Color color) {
        this.backColor = color;
    }

    public Color getBackColor() {
        return backColor;
    }

    public String getInfo() {
        if(super.getInfo().equals("")) {
            return "";
        }
        else {
            return super.getInfo() + " " +
                    "backColor:" + backColor.toString();
        }
    }
}
