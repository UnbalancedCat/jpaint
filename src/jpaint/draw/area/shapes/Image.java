package jpaint.draw.area.shapes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class Image extends Shape{

    final BufferedImage image;

    public Image(int id, String name, int type, BufferedImage image, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd) {
        super(id, name,type, xPosStart, yPosStart, xPosEnd, yPosEnd);
        this.image = image;
    }

    @Override
    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        super.highlight(g2, isHighlighted);
        g2.drawImage(image, xPosStart, yPosStart, xPosEnd, yPosEnd, null);
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        return false;
    }

    @Override
    public ShapeRecord getRecord() {
        return null;
    }

    @Override
    public <T extends ShapeRecord> void recover(T e) {

    }

    @Override
    public void setBackColor(Color color) {

    }

    @Override
    public Color getBackColor() {
        return null;
    }

    public String getInfo() {
        if(super.getInfo().equals("")) {
            return "";
        }
        else {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try {
                ImageIO.write(image, "png", stream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            String imageString = Base64.getEncoder().encodeToString(stream.toByteArray());

            return super.getInfo() + " " +
                    "image:" + imageString;
        }
    }
}
