package jpaint.draw.area.shapes;

import java.awt.*;

public class Text extends Shape {

    protected Color backColor;
    protected String text;
    protected Font font;
    protected int textLength;

    public Text(int id, String name, int type, int xPos, int yPos, Color foreColor, Color backColor, float stroke, boolean isFilled, String text, Font font) {
        super(id, name, type, xPos, yPos, foreColor, stroke, isFilled);
        this.backColor = backColor;
        this.text = text;
        this.font = font;
        for(int i = 0; i < text.length(); i++) {
            int ascii = Character.codePointAt(text, i);
            if (ascii >= 0 && ascii <= 255)
                textLength++;
            else
                textLength += 2;
        }
    }

    public Text(int id, String name, int type, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color foreColor, Color backColor, float stroke, boolean isFilled, String text, Font font) {
        super(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, foreColor, stroke, isFilled);
        this.backColor = backColor;
        this.text = text;
        this.font = font;
        for(int i = 0; i < text.length(); i++) {
            int ascii = Character.codePointAt(text, i);
            if (ascii >= 0 && ascii <= 255)
                textLength++;
            else
                textLength += 2;
        }
    }

    @Override
    public void drawShape(Graphics2D g2, boolean isHighlighted) {
        if (isHighlighted) {
            g2.setColor(Color.BLACK);
            BasicStroke stroke1 = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0f, new float[] { 10, 10 }, 0);
            g2.setStroke(stroke1);
            if (isFilled) {
                g2.drawRect((int) (Math.min(xPosStart, xPosEnd) - stroke), (int) (Math.min(yPosStart, yPosEnd) - stroke), (int) (Math.abs(xPosStart - xPosEnd) + stroke * 2), (int) (Math.abs(yPosStart - yPosEnd) + stroke * 2));
            } else {
                int xTextPosEnd = calcXPos();
                int yTextPosEnd = calcYPos();
                g2.drawRect(Math.min(xTextPosEnd, xPosEnd), Math.min(yTextPosEnd, yPosEnd), Math.abs(xTextPosEnd - xPosEnd), Math.abs(yTextPosEnd - yPosEnd));
            }
        }
        if (isFilled) {
            g2.setColor(backColor);
            g2.fillRect(Math.min(xPosStart, xPosEnd), Math.min(yPosStart, yPosEnd), (Math.abs(xPosStart - xPosEnd)), (Math.abs(yPosStart - yPosEnd)));
        }
        g2.setStroke(new BasicStroke(stroke));
        g2.setColor(color);
        g2.setFont(new Font(font.getFontName(), font.getStyle(), font.getSize() == 0 ? Math.abs(xPosStart - xPosEnd) / textLength * 2 : font.getSize()));
        int xLength = g2.getFont().getSize() * textLength / 2;
        int yLength = g2.getFont().getSize();
        g2.drawString(text, xPosStart <= xPosEnd ? xPosEnd - xLength : xPosEnd, yPosStart <= yPosEnd ? yPosEnd : yPosEnd + yLength);
    }

    @Override
    public boolean isInShape(int xPos, int yPos) {
        int xTextPosEnd = calcXPos();
        int yTextPosEnd = calcYPos();
        if (isFilled && xPos <= Math.max(xPosStart, xPosEnd) && xPos >= Math.min(xPosStart, xPosEnd) && yPos <= Math.max(yPosStart, yPosEnd) && yPos >= Math.min(yPosStart, yPosEnd)) {
            return true;
        } else return xPos <= Math.max(xTextPosEnd, xPosEnd) && xPos >= Math.min(xTextPosEnd, xPosEnd) && yPos <= Math.max(yTextPosEnd, yPosEnd) && yPos >= Math.min(yTextPosEnd, yPosEnd);
    }

    public static class TextRecord extends ShapeRecord {
        protected final Color backColor;
        protected final String text;
        protected final Font font;
        protected int textLength;

        protected TextRecord(String shapeName, int xPosStart, int yPosStart, int xPosEnd, int yPosEnd, Color color, Color backColor, float stroke, boolean isFilled, String text, Font font) {
            super(shapeName, 4, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke, isFilled);
            this.backColor = backColor;
            this.text = text;
            this.font = font;
            for(int i = 0; i < text.length(); i++) {
                int ascii = Character.codePointAt(text, i);
                if (ascii >= 0 && ascii <= 255)
                    textLength++;
                else
                    textLength += 2;
            }
        }
    }

    private int calcXPos() {
        int xLength = (font.getSize() == 0 ? Math.abs(xPosStart - xPosEnd) / textLength * 2 : font.getSize()) * textLength / 2;
        return xPosStart <= xPosEnd ? xPosEnd - xLength : xPosEnd + xLength;
    }

    private int calcYPos() {
        int yLength_1 = (font.getSize() == 0 ? Math.abs(xPosStart - xPosEnd) / textLength * 2 : font.getSize());
        return yPosStart <= yPosEnd ? yPosEnd - yLength_1 : yPosEnd + yLength_1;
    }

    @Override
    public ShapeRecord getRecord() {
        return new TextRecord(name, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backColor, stroke, isFilled, text, font);
    }

    @Override
    public <T extends ShapeRecord> void recover(T e) {
        super.recover(e);
        if(e instanceof TextRecord record) {
            this.backColor = record.backColor;
            this.text = record.text;
            this.font = record.font;
        }
    }

    @Override
    public void setBackColor(Color color) {
        this.backColor = color;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void setTextLength(int textLength) {
        this.textLength = textLength;
    }

    public Color getBackColor() {
        return backColor;
    }

    public String getText() {
        return text;
    }

    public Font getFont() {
        return font;
    }

    public String getInfo() {
        if(super.getInfo().equals("")) {
            return "";
        }
        else {
            return super.getInfo() + " " +
                    "backColor:" + backColor.toString() + " " +
                    "text:" + text.replace(" ", "&nbsp;") + " " +
                    "font:" + font.toString();
        }
    }
}