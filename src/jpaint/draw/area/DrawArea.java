package jpaint.draw.area;

import jpaint.draw.area.brushes.Pencil;
import jpaint.draw.area.shapes.Image;
import jpaint.draw.area.shapes.Shape;
import jpaint.draw.area.shapes.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class DrawArea extends JPanel {
    protected boolean save = true;
    protected boolean isDragged = false;
    protected int canvasWidth = 800;
    protected int canvasHeight = 450;
    protected final CanvasPanel canvas = new CanvasPanel();

    public enum selectType {LINE, CIRCLE, OVAL, RECT, TEXT, PENCIL, ERASER, IMAGE, BUCKET, ABSORBER, SELECT}

    protected selectType type = selectType.PENCIL;
    protected Shape shapeCurrent = null;
    protected Color foreColor = Color.BLACK;
    protected Color backColor = Color.WHITE;
    protected Color pencilColor = Color.BLACK;
    protected float stroke = 3;
    protected boolean filled = false;
    protected String text = "Ĭ����������";
    protected String fontName = "����";
    protected int fontStyle = Font.PLAIN;
    protected int fontSize = 0;

    protected final Stack<opsRecord> undoStack = new Stack<>();
    protected final Stack<opsRecord> redoStack = new Stack<>();
    protected final Vector<Shape> shapeList = new Vector<>();
    protected final Vector<Shape> shapeListSearched = new Vector<>();
    public enum recordType {ADD, DELETE, UPDATE}
    protected int shapeCount = 1;

    public DrawArea() {
        shapeList.add(new Rect(0, "background", selectType.RECT.ordinal(), 0, 0, canvasWidth, canvasHeight, Color.WHITE, Color.WHITE, 0, true));
        canvas.setPreferredSize(new Dimension(canvasWidth, canvasHeight));

        this.setPreferredSize(new Dimension(canvasWidth + 100, canvasHeight + 50));
        this.setBackground(new Color(202, 212, 227));
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.add(canvas);

    }

    public class CanvasPanel extends JComponent implements Serializable {

        public CanvasPanel() {
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    shapeListSearched.clear();
                    String name = typeToName(type) + shapeCount;
                    switch (type) {
                        case PENCIL ->
                                shapeCurrent = new Pencil(shapeCount, name, type.ordinal(), e.getX(), e.getY(), pencilColor, stroke);
                        case ERASER ->
                                shapeCurrent = new Pencil(shapeCount, name, type.ordinal(), e.getX(), e.getY(), backColor, stroke);
                        case TEXT ->
                                shapeCurrent = new Text(shapeCount, name, type.ordinal(), e.getX(), e.getY(), foreColor, backColor, stroke, filled, text, new Font(fontName, fontStyle, fontSize));
                        case LINE ->
                                shapeCurrent = new Line(shapeCount, name, 0, e.getX(), e.getY(), foreColor, stroke);
                        case CIRCLE ->
                                shapeCurrent = new Circle(shapeCount, name, type.ordinal(), e.getX(), e.getY(), foreColor, backColor, stroke, filled);
                        case OVAL ->
                                shapeCurrent = new Oval(shapeCount, name, type.ordinal(), e.getX(), e.getY(), foreColor, backColor, stroke, filled);
                        case RECT ->
                                shapeCurrent = new Rect(shapeCount, name, type.ordinal(), e.getX(), e.getY(), foreColor, backColor, stroke, filled);
                        case BUCKET -> {
                            if (shapeCurrent != null && !Objects.equals(shapeCurrent.getName(), "") && shapeCurrent.isInShape(e.getX(), e.getY())) {
                                undoStack.push(new opsRecord(recordType.UPDATE, shapeCurrent.getId(), shapeCurrent.getRecord()));
                                shapeCurrent.setBackColor(backColor);
                                shapeCurrent.setFilled(true);
                                redoStack.clear();
                                save = false;
                            } else {
                                shapeCurrent = null;
                                for (Shape shape : shapeList) {
                                    if (shape.isInShape(e.getX(), e.getY())) {
                                        undoStack.push(new opsRecord(recordType.UPDATE, shape.getId(), shape.getRecord()));
                                        shape.setBackColor(backColor);
                                        shape.setFilled(true);
                                        shapeCurrent = shape;
                                        redoStack.clear();
                                        save = false;
                                        break;
                                    }
                                }
                            }
                        }
                        case ABSORBER, SELECT -> {
                            type = selectType.SELECT;
                            if (shapeCurrent != null && !Objects.equals(shapeCurrent.getName(), "")  && shapeCurrent.isInShape(e.getX(), e.getY())) {
                                undoStack.push(new opsRecord(recordType.UPDATE, shapeCurrent.getId(), shapeCurrent.getRecord()));
                                shapeCurrent.setPosMouse(e.getX(), e.getY());
                                shapeListSearched.add(shapeCurrent);
                            } else {
                                shapeCurrent = null;
                                for (int i = shapeList.size() - 1; i >= 0; i--) {
                                    if (shapeList.get(i).isInShape(e.getX(), e.getY())) {
                                        shapeList.get(i).setPosMouse(e.getX(), e.getY());
                                        shapeListSearched.add(shapeList.get(i));
                                        shapeCurrent = shapeList.get(i);
                                        break;
                                    }
                                }
                                if (shapeCurrent != null) {
                                    undoStack.push(new opsRecord(recordType.UPDATE, shapeCurrent.getId(), shapeCurrent.getRecord()));
                                }
                            }
                        }
                    }
                    if (type != selectType.BUCKET && type != selectType.SELECT) {
                        shapeList.add(shapeCurrent);
                        shapeCount++;
                        assert shapeCurrent != null;
                        undoStack.push(new opsRecord(recordType.ADD, shapeCurrent.getId(), shapeCurrent.getRecord()));
                        shapeListSearched.add(shapeCurrent);
                    }
                    if (type != selectType.BUCKET) {
                        save = false;
                    }
                    isDragged = false;
                    repaint();
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (!isDragged()) {
                        if (type == selectType.CIRCLE || type == selectType.OVAL || type == selectType.RECT || type == selectType.TEXT) {
                            shapeList.removeElement(shapeList.lastElement());
                            shapeCount--;
                            undoStack.pop();
                            save = true;
                            shapeCurrent = null;
                        } else if (type == selectType.SELECT && shapeCurrent != null) {
                            undoStack.pop();
                            shapeListSearched.clear();
                            save = true;
                        }
                    }
                    repaint();
                }
            });
            this.addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    switch (type) {
                        case PENCIL -> {
                            ((Pencil) shapeCurrent).add(new Line(shapeCurrent.getXPosCurrent(), shapeCurrent.getYPosCurrent(), e.getX(), e.getY(), pencilColor, stroke));
                            shapeCurrent.setPosCurrent(e.getX(), e.getY());
                        }
                        case ERASER -> {
                            ((Pencil) shapeCurrent).add(new Line(shapeCurrent.getXPosCurrent(), shapeCurrent.getYPosCurrent(), e.getX(), e.getY(), backColor, stroke));
                            shapeCurrent.setPosCurrent(e.getX(), e.getY());
                        }
                        case TEXT, LINE, CIRCLE, OVAL, RECT -> shapeCurrent.setPosCurrent(e.getX(), e.getY());
                        case SELECT -> {
                            if (shapeCurrent != null) {
                                shapeCurrent.setPosChange(e.getX(), e.getY());
                            }
                        }
                    }
                    if (shapeCurrent != null) {
                        redoStack.clear();
                    }
                    isDragged = true;
                    repaint();
                }
            });
            setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        }

        public Stack<opsRecord> getUndoStack() {
            return undoStack;
        }

        public Stack<opsRecord> getRedoStack() {
            return redoStack;
        }

        public RenderedImage getImage() {
            BufferedImage image = new BufferedImage(canvasWidth, canvasHeight, BufferedImage.TYPE_INT_RGB);
            Graphics2D g2 = image.createGraphics();
            g2.fillRect(0, 0, image.getWidth(), image.getHeight());
            paint(g2);
            return image;
        }

        @Override
        public void paint(Graphics g) {
            Graphics2D g2 = (Graphics2D) g;
            for (Shape shape : shapeList) {
                if (!Objects.equals(shape.getName(), "")) {
                    shape.drawShape(g2, false);
                }
            }
            if (shapeCurrent != null && !Objects.equals(shapeCurrent.getName(), "")) {
                shapeCurrent.drawShape(g2, false);
            }
            for (Shape shape : shapeListSearched) {
                if (!Objects.equals(shape.getName(), "")) {
                    shape.drawShape(g2, true);
                }
            }
        }
    }


    public String getInfo() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("canvasWidth:").append(canvasWidth).append("\n");
        buffer.append("canvasHeight:").append(canvasHeight).append("\n");
        buffer.append("shapeList:{\n");
        for(Shape shape: shapeList){
            if(shape.getInfo().equals("")) continue;
            buffer.append(shape.getInfo()).append("\n");
        }
        buffer.append("}\n");
        buffer.append(Calendar.getInstance().getTime());
        return buffer.toString();
    }

    public void setInfo() {
        undoStack.clear();
        redoStack.clear();
        shapeList.clear();
        canvasHeight = 450;
        canvasWidth = 800;
        shapeCount = 1;
        shapeList.add(new Rect(0, "background", selectType.RECT.ordinal(), 0, 0, canvasWidth, canvasHeight, backColor, backColor, 0, true));
        save = true;
        isDragged = false;
        shapeCurrent = null;
    }
    public void setInfo(BufferedImage image) {
        undoStack.clear();
        redoStack.clear();
        shapeList.clear();
        canvasHeight = image.getHeight();
        canvasWidth = image.getWidth();
        shapeCount = 1;
        shapeList.add(new Image(0, "background", selectType.IMAGE.ordinal(), image, 0, 0, canvasWidth, canvasHeight));
        save = true;
        isDragged = false;
        shapeCurrent = null;
    }
    public void setInfo(String[] info) {
        undoStack.clear();
        redoStack.clear();
        shapeList.clear();
        canvasWidth = Integer.parseInt(info[0].replace("canvasWidth:", ""));
        canvasHeight = Integer.parseInt(info[1].replace("canvasHeight:", ""));
        for(int i = 3; i < info.length - 2; i++) {
            Shape addShape = null;
            if(info[i].equals("}")) break;
            else {
                String[] colorBuffer;

                String[] shape = info[i].split(" ");
                int id = Integer.parseInt(shape[0].replace("id:", ""));
                String name = shape[1].replace("name:", "");
                int type = Integer.parseInt(shape[2].replace("type:", ""));
                int xPosStart = Integer.parseInt(shape[3].replace("xPosStart:", ""));
                int yPosStart = Integer.parseInt(shape[4].replace("yPosStart:", ""));
                int xPosEnd = Integer.parseInt(shape[5].replace("xPosEnd:", ""));
                int yPosEnd = Integer.parseInt(shape[6].replace("yPosEnd:", ""));

                colorBuffer = shape[7].split(",");
                Color color = new Color(Integer.parseInt(colorBuffer[0].replace("color:java.awt.Color[r=", "")), Integer.parseInt(colorBuffer[1].replace("g=", "")), Integer.parseInt(colorBuffer[2].replace("b=", "").replace("]", "")));
                float stroke = Float.parseFloat(shape[8].replace("stroke:", ""));
                boolean isFilled = Boolean.parseBoolean(shape[9].replace("isFilled:", ""));

                if(type == 0) {
                    addShape = new Line(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke);
                }
                else if(type == 1 || type == 2 || type == 3 || type == 4){
                    colorBuffer = shape[10].split(",");
                    Color backcolor = new Color(Integer.parseInt(colorBuffer[0].replace("backColor:java.awt.Color[r=", "")), Integer.parseInt(colorBuffer[1].replace("g=", "")), Integer.parseInt(colorBuffer[2].replace("b=", "").replace("]", "")));
                    switch (type) {
                        case 1 ->
                                addShape = new Circle(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backcolor, stroke, isFilled);
                        case 2 ->
                                addShape = new Oval(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backcolor, stroke, isFilled);
                        case 3 ->
                                addShape = new Rect(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backcolor, stroke, isFilled);
                        case 4 -> {
                            String text = shape[11].replace("text:", "").replace("&nbsp;", " ");
                            String[] fontBuffer = shape[12].split(",");
                            String fontStyle = fontBuffer[2].replace("style=", "");

                            Font font = new Font(fontBuffer[1].replace("name=", ""), fontStyle.equals("italic") ? Font.ITALIC : fontStyle.equals("bold") ? Font.BOLD : Font.PLAIN , Integer.parseInt(fontBuffer[3].replace("size=", "").replace("]", "")));
                            addShape = new Text(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, backcolor, stroke, isFilled, text, font);
                        }
                    }
                }
                else if(type == 5 || type == 6) {
                    addShape = new Pencil(id, name, type, xPosStart, yPosStart, xPosEnd, yPosEnd, color, stroke);
                    for(int j = 10; j < shape.length - 10; j += 4) {
                        int xPosStartLine = Integer.parseInt(shape[j].replace("(", ""));
                        int yPosStartLine = Integer.parseInt(shape[j + 1]);
                        int xPosEndLine = Integer.parseInt(shape[j + 2]);
                        int yPosEndLine = Integer.parseInt(shape[j + 3].replace(")", ""));

                        ((Pencil)addShape).add(new Line(xPosStartLine, yPosStartLine, xPosEndLine, yPosEndLine, color, stroke));
                    }
                }
                else if(type == 7) {
                    String imageString = shape[10].replace("image:", "");
                    ByteArrayInputStream stream = new ByteArrayInputStream(Base64.getDecoder().decode(imageString));
                    try {
                        BufferedImage image = ImageIO.read(stream);
                        addShape = new Image(0, "background", type, image, 0, 0, canvasWidth, canvasHeight);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                shapeList.add(addShape);
                this.shapeCount = id + 1;
            }
        }
        save = true;
        isDragged = false;
        shapeCurrent = null;
    }

    public void setType(selectType type) {
        this.type = type;
    }

    public Color getForeColor() {
        return foreColor;
    }

    public void setForeColor(Color foreColor) {
        this.foreColor = foreColor;
    }

    public void setBackColor(Color backColor) {
        this.backColor = backColor;
    }

    public void setPencilColor(Color pencilColor) {
        this.pencilColor = pencilColor;
    }

    public void setStroke(Float stroke) {
        this.stroke = stroke;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    public void setFontStyle(int fontStyle) {
        this.fontStyle = fontStyle;
    }

    public boolean isNotSave() {
        return !save;
    }

    public void setSave(boolean save) {
        this.save = save;
    }

    public record opsRecord(recordType recordType, int shapeId, Shape.ShapeRecord shapeRecord) { }

    public Vector<Shape> getShapeList() {
        return shapeList;
    }

    public Vector<Shape> getShapeListSearched() {
        return shapeListSearched;
    }

    public CanvasPanel getCanvas() {
        return canvas;
    }

    public selectType getType() {
        return type;
    }

    public Shape getShapeCurrent() {
        return shapeCurrent;
    }

    public boolean isDragged() {
        return isDragged;
    }

    public Color colorTaker(){
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        Color color = null;
        if (robot != null) {
            color = robot.getPixelColor(MouseInfo.getPointerInfo().getLocation().x,
                    MouseInfo.getPointerInfo().getLocation().y);
        }
        return color;
    }

    public String typeToName(selectType type) {
        return switch (type) {
            case LINE -> "ֱ��";
            case CIRCLE -> "Բ";
            case OVAL -> "��Բ";
            case RECT -> "����";
            case TEXT -> "����";
            case PENCIL -> "Ǧ��";
            case ERASER -> "��Ƥ";
            case ABSORBER -> "ȡɫ��";
            default -> null;
        };
    }
}
