package jpaint;

import jpaint.draw.area.DrawArea;
import jpaint.draw.area.shapes.Shape;
import jpaint.file.io.FileIO;
import jpaint.menu.bar.MenuBar;
import jpaint.menu.bar.tabs.file.units.AboutDialog;
import jpaint.menu.bar.tabs.home.units.ColorPanel;
import jpaint.menu.bar.tabs.home.units.ShapesPanel;
import jpaint.shape.manager.ShapeManager;
import jpaint.shape.manager.units.ShapeEdit;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Objects;
import java.util.Vector;

public class Listeners {

    public Listeners(JPaint jPaint, FileIO fileIO, MenuBar menuBar, DrawArea drawArea, ShapeManager shapeManager) {
        menuBar.getHomePanel().getToolsPanel().getPencil().addActionListener(e -> drawArea.setType(DrawArea.selectType.PENCIL));
        menuBar.getHomePanel().getToolsPanel().getEraser().addActionListener(e -> drawArea.setType(DrawArea.selectType.ERASER));
        menuBar.getHomePanel().getToolsPanel().getText().addActionListener(e -> drawArea.setType(DrawArea.selectType.TEXT));
        menuBar.getHomePanel().getToolsPanel().getAbsorber().addActionListener(e -> drawArea.setType(DrawArea.selectType.ABSORBER));
        menuBar.getHomePanel().getToolsPanel().getZoom().addActionListener(e -> drawArea.setType(DrawArea.selectType.SELECT));
        menuBar.getHomePanel().getToolsPanel().getBucket().addActionListener(e -> drawArea.setType(DrawArea.selectType.BUCKET));

        menuBar.getHomePanel().getShapesPanel().getLine().addActionListener(e -> drawArea.setType(DrawArea.selectType.LINE));
        menuBar.getHomePanel().getShapesPanel().getCircle().addActionListener(e -> drawArea.setType(DrawArea.selectType.CIRCLE));
        menuBar.getHomePanel().getShapesPanel().getOval().addActionListener(e -> drawArea.setType(DrawArea.selectType.OVAL));
        menuBar.getHomePanel().getShapesPanel().getRect().addActionListener(e -> drawArea.setType(DrawArea.selectType.RECT));

        menuBar.getHomePanel().getStrokePanel().getWid().addItemListener(e -> drawArea.setStroke((Float) e.getItem()));

        menuBar.getHomePanel().getColorPanel().getEditColor().addActionListener(e -> {
            Color result = JColorChooser.showDialog(drawArea, "编辑颜色", Color.BLACK);
            if (result == null)
                return;
            if(menuBar.getHomePanel().getColorPanel().getCurrentSelectColor() == ColorPanel.selectColor.FORECOLOR) {
                menuBar.getHomePanel().getColorPanel().getForeColorButton().setBackground(result);
                drawArea.setPencilColor(result);
                if(menuBar.getHomePanel().getShapesPanel().getOutlineTypeSelect() == ShapesPanel.outlineType.COLORED) {
                    drawArea.setForeColor(result);
                } else if (menuBar.getHomePanel().getShapesPanel().getOutlineTypeSelect() == ShapesPanel.outlineType.NULL) {
                    drawArea.setForeColor(new Color(result.getRed(), result.getGreen(), result.getBlue(), 0));
                }
            } else if (menuBar.getHomePanel().getColorPanel().getCurrentSelectColor() == ColorPanel.selectColor.BACKCOLOR) {
                menuBar.getHomePanel().getColorPanel().getBackColorButton().setBackground(result);
                drawArea.setBackColor(result);
            }
        });
        menuBar.getHomePanel().getColorPanel().getForeColorButton().addActionListener(e -> menuBar.getHomePanel().getColorPanel().setCurrentSelectColor(ColorPanel.selectColor.FORECOLOR));
        menuBar.getHomePanel().getColorPanel().getBackColorButton().addActionListener(e -> menuBar.getHomePanel().getColorPanel().setCurrentSelectColor(ColorPanel.selectColor.BACKCOLOR));

        menuBar.getHomePanel().getShapesPanel().getOutline().addItemListener(e -> {
            if ("纯色".equals(e.getItem())) {
                drawArea.setForeColor(menuBar.getHomePanel().getColorPanel().getForeColorButton().getBackground());
                menuBar.getHomePanel().getShapesPanel().setOutlineTypeSelect(ShapesPanel.outlineType.COLORED);
            } else if("无轮廓线".equals(e.getItem())) {
                drawArea.setForeColor(new Color(drawArea.getForeColor().getRed(), drawArea.getForeColor().getGreen(), drawArea.getForeColor().getBlue(),0));
                drawArea.setPencilColor(menuBar.getHomePanel().getColorPanel().getForeColorButton().getBackground());
                menuBar.getHomePanel().getShapesPanel().setOutlineTypeSelect(ShapesPanel.outlineType.NULL);
            }
        });
        menuBar.getHomePanel().getShapesPanel().getFill().addItemListener(e -> {
            if ("纯色".equals(e.getItem())) {
                drawArea.setFilled(true);
            } else if("不填充".equals(e.getItem())) {
                drawArea.setFilled(false);
            }
        });

        menuBar.getHomePanel().getTextPanel().getFontName().addItemListener(e -> drawArea.setFontName((String) e.getItem()));
        menuBar.getHomePanel().getTextPanel().getFontSize().addItemListener(e -> {
            int fontSize = switch ((String) e.getItem()) {
                case "小初" -> 48;
                case "四号" -> 18;
                case "五号" -> 14;
                case "10" -> 10;
                case "36" -> 36;
                default -> 0;
            };
            drawArea.setFontSize(fontSize);
        });
        menuBar.getHomePanel().getTextPanel().getFontStyle()[0].addItemListener(e -> {
            JCheckBox cbBold = (JCheckBox) e.getItem();
            if (cbBold.isSelected()) {
                cbBold.setIcon(new ImageIcon("image\\boldSelected.png"));
            } else {
                cbBold.setIcon(new ImageIcon("image\\bold.png"));
            }
            int fontStyle = (cbBold.isSelected() ? 1 : 0) + (menuBar.getHomePanel().getTextPanel().getFontStyle()[1].isSelected() ? 2 : 0);
            drawArea.setFontStyle(fontStyle);
        });
        menuBar.getHomePanel().getTextPanel().getFontStyle()[1].addItemListener(e -> {
            JCheckBox cbItalic = (JCheckBox) e.getItem();
            if (cbItalic.isSelected()) {
                cbItalic.setIcon(new ImageIcon("image\\italicSelected.png"));
            } else {
                cbItalic.setIcon(new ImageIcon("image\\italic.png"));
            }
            int fontStyle = (menuBar.getHomePanel().getTextPanel().getFontStyle()[0].isSelected() ? 1 : 0) + (cbItalic.isSelected() ? 2 : 0);
            drawArea.setFontStyle(fontStyle);
        });
        menuBar.getHomePanel().getTextPanel().getTextField().addTextListener(e -> {
            TextField textField = (TextField) e.getSource();
            if (!Objects.equals(textField.getText(), "")) {
                drawArea.setText(textField.getText());
            } else {
                drawArea.setText("默认文字内容");
            }
        });

        menuBar.getEditPanel().getUndoItem().addActionListener(e -> {
            if(drawArea.getCanvas().getUndoStack().size() > 0) {
                DrawArea.opsRecord record = drawArea.getCanvas().getUndoStack().pop();
                drawArea.getCanvas().getRedoStack().push(new DrawArea.opsRecord(record.recordType(), record.shapeId(), drawArea.getShapeList().get(record.shapeId()).getRecord()));
                if (record.recordType() == DrawArea.recordType.ADD) {
                    drawArea.getShapeList().get(record.shapeId()).setName("");
                } else if(record.recordType() == DrawArea.recordType.DELETE) {
                    drawArea.getShapeList().get(record.shapeId()).recover(record.shapeRecord());
                } else {
                    drawArea.getShapeList().get(record.shapeId()).recover(record.shapeRecord());
                }
            }
            drawArea.setSave(false);
            drawArea.getCanvas().repaint();
            shapeManager.setShapes();
        });
        menuBar.getEditPanel().getRedoItem().addActionListener(e -> {
            if(drawArea.getCanvas().getRedoStack().size() > 0) {
                DrawArea.opsRecord record = drawArea.getCanvas().getRedoStack().pop();
                drawArea.getCanvas().getUndoStack().push(new DrawArea.opsRecord(record.recordType(), record.shapeId(), drawArea.getShapeList().get(record.shapeId()).getRecord()));
                if (record.recordType() == DrawArea.recordType.ADD) {
                    drawArea.getShapeList().get(record.shapeId()).recover(record.shapeRecord());
                } else if(record.recordType() == DrawArea.recordType.DELETE) {
                    drawArea.getShapeList().get(record.shapeId()).setName("");
                } else {
                    drawArea.getShapeList().get(record.shapeId()).recover(record.shapeRecord());
                }
            }
            drawArea.setSave(false);
            drawArea.getCanvas().repaint();
            shapeManager.setShapes();
        });
        menuBar.getEditPanel().getClearItem().addActionListener(e -> {
            while(drawArea.getCanvas().getUndoStack().size() > 0) {
                DrawArea.opsRecord record = drawArea.getCanvas().getUndoStack().pop();
                drawArea.getCanvas().getRedoStack().push(new DrawArea.opsRecord(record.recordType(), record.shapeId(), drawArea.getShapeList().get(record.shapeId()).getRecord()));
            }
            for (int i = 1; i < drawArea.getShapeList().size(); i++) {
                drawArea.getShapeList().get(i).setName("");
            }
            drawArea.setSave(false);
            drawArea.getCanvas().repaint();
            shapeManager.setShapes();
        });

        menuBar.getFilePanel().getAboutItem().addActionListener(e -> new AboutDialog(jPaint));
        menuBar.getFilePanel().getExitItem().addActionListener(e -> {
            if(drawArea.isNotSave()) {
                switch(JOptionPane.showConfirmDialog(jPaint, "你想将更改保存到 " + jPaint.getFileName() + " 吗？", "画图", JOptionPane.YES_NO_CANCEL_OPTION)) {
                    case JOptionPane.YES_OPTION:
                        if(jPaint.getFilePath() != null) {
                            fileIO.saveFile(jPaint, drawArea);
                        }
                        else if(fileIO.saveFile(jPaint, drawArea) == null) {
                            return;
                        }
                    case JOptionPane.NO_OPTION:
                        break;
                    case JOptionPane.CANCEL_OPTION:
                        return;
                }
            }
            System.exit(0);
        });
        menuBar.getFilePanel().getSaveItem().addActionListener(e -> {
            if(drawArea.isNotSave()) {
                if(fileIO.saveFile(jPaint, drawArea) != null) {
                    drawArea.setSave(true);
                }
            }
        });
        menuBar.getFilePanel().getSaveAsItem().addActionListener(e -> {
                if(fileIO.saveAsFile(jPaint, drawArea) != null) {
                    drawArea.setSave(true);
            }
        });
        menuBar.getFilePanel().getOpenItem().addActionListener(e -> {
            if(drawArea.isNotSave()) {
                switch(JOptionPane.showConfirmDialog(jPaint, "你想将更改保存到 " + jPaint.getFileName() + " 吗？", "画图", JOptionPane.YES_NO_CANCEL_OPTION)) {
                    case JOptionPane.YES_OPTION:
                        if(jPaint.getFilePath() != null) {
                            fileIO.saveFile(jPaint, drawArea);
                        }
                        else if(fileIO.saveFile(jPaint, drawArea) == null) {
                            return;
                        }
                    case JOptionPane.NO_OPTION:
                        break;
                    case JOptionPane.CANCEL_OPTION:
                        return;
                }
            }
            fileIO.openFile(jPaint, drawArea);
            shapeManager.setShapes(drawArea.getShapeList());
            jPaint.repaint();
        });
        menuBar.getFilePanel().getNewItem().addActionListener(e -> {
            if(drawArea.isNotSave()) {
                switch(JOptionPane.showConfirmDialog(jPaint, "你想将更改保存到 " + jPaint.getFileName() + " 吗？", "画图", JOptionPane.YES_NO_CANCEL_OPTION)) {
                    case JOptionPane.YES_OPTION:
                        if(jPaint.getFilePath() != null) {
                            fileIO.saveFile(jPaint, drawArea);
                        }
                        else if(fileIO.saveFile(jPaint, drawArea) == null) {
                            return;
                        }
                    case JOptionPane.NO_OPTION:
                        break;
                    case JOptionPane.CANCEL_OPTION:
                        return;
                }
            }
            fileIO.newFile(jPaint, drawArea);
            shapeManager.setShapes(drawArea.getShapeList());
        });

        jPaint.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        jPaint.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                if(drawArea.isNotSave()) {
                    switch(JOptionPane.showConfirmDialog(jPaint, "你想将更改保存到 " + jPaint.getFileName() + " 吗？", "画图", JOptionPane.YES_NO_CANCEL_OPTION)) {
                        case JOptionPane.YES_OPTION:
                            if(jPaint.getFilePath() != null) {
                                fileIO.saveFile(jPaint, drawArea);
                            }
                            else if(fileIO.saveAsFile(jPaint, drawArea) == null) {
                                return;
                            }
                        case JOptionPane.NO_OPTION:
                            break;
                        case JOptionPane.CANCEL_OPTION:
                            return;
                    }
                }
                System.exit(0);
            }
        });

        drawArea.getCanvas().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                DrawArea.selectType type = drawArea.getType();
                if (type == DrawArea.selectType.LINE || type == DrawArea.selectType.TEXT){
                    shapeManager.addShape(drawArea.getShapeCurrent());
                    shapeManager.setShapes();
                } else if(type == DrawArea.selectType.CIRCLE || type == DrawArea.selectType.OVAL || type == DrawArea.selectType.RECT) {
                    if (drawArea.isDragged()) {
                        shapeManager.addShape(drawArea.getShapeCurrent());
                        shapeManager.setShapes();
                    }
                }
            }
        });

        drawArea.getCanvas().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if (drawArea.getType() == DrawArea.selectType.ABSORBER) {
                    if (menuBar.getHomePanel().getColorPanel().getCurrentSelectColor() == ColorPanel.selectColor.FORECOLOR) {
                        menuBar.getHomePanel().getColorPanel().getForeColorButton().setBackground(drawArea.colorTaker());
                        drawArea.setPencilColor(drawArea.colorTaker());
                        if (menuBar.getHomePanel().getShapesPanel().getOutlineTypeSelect() == ShapesPanel.outlineType.COLORED) {
                            drawArea.setForeColor(drawArea.colorTaker());
                        } else if (menuBar.getHomePanel().getShapesPanel().getOutlineTypeSelect() == ShapesPanel.outlineType.NULL) {
                            drawArea.setForeColor(new Color(drawArea.colorTaker().getRed(), drawArea.colorTaker().getGreen(), drawArea.colorTaker().getBlue(), 0));
                        }
                    } else if (menuBar.getHomePanel().getColorPanel().getCurrentSelectColor() == ColorPanel.selectColor.BACKCOLOR) {
                        menuBar.getHomePanel().getColorPanel().getBackColorButton().setBackground(drawArea.colorTaker());
                        drawArea.setBackColor(drawArea.colorTaker());
                    }
                }
            }
        });

        shapeManager.getSearchButton().addActionListener(e -> {
            drawArea.getShapeListSearched().clear();
            String name = shapeManager.getNameField().getText();
            if (Objects.equals(name, "")) {
                JOptionPane.showMessageDialog(null, "请输入要查找的形状名");
            }
            Vector<Shape> shapeList = new Vector<>();
            for (int i = 1; i < drawArea.getShapeList().size(); i++) {
                if (drawArea.getShapeList().get(i).getName().contains(name)) {
                    if (Objects.equals(drawArea.getShapeList().get(i).getName(), name)) {
                        drawArea.setType(DrawArea.selectType.SELECT);
                        drawArea.getShapeListSearched().add(drawArea.getShapeList().get(i));
                    }
                    shapeList.add(drawArea.getShapeList().get(i));
                }
            }
            if (shapeList.size() > 0) {
                shapeManager.addShapeTab(name, shapeList);
            } else {
                JOptionPane.showMessageDialog(null, "查找失败");
            }
            drawArea.getCanvas().repaint();
        });

        shapeManager.getEditButton().addActionListener(e -> {
            String name = shapeManager.getNameField().getText();
            if (Objects.equals(name, "")) {
                if (drawArea.getShapeCurrent() != null && drawArea.getShapeCurrent().getType() != DrawArea.selectType.PENCIL.ordinal() && drawArea.getShapeCurrent().getType() != DrawArea.selectType.ERASER.ordinal() && !Objects.equals(drawArea.getShapeCurrent().getName(), "")) {
                    new ShapeEdit(drawArea, shapeManager, drawArea.getShapeCurrent());
                } else {
                    JOptionPane.showMessageDialog(null, "请输入形状名或选中要修改的形状", "查找失败", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                boolean flag = false;
                for (int i = 1; i < drawArea.getShapeList().size(); i++) {
                    if (Objects.equals(drawArea.getShapeList().get(i).getName(), name)) {
                        new ShapeEdit(drawArea, shapeManager, drawArea.getShapeList().get(i));
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    JOptionPane.showMessageDialog(null, "不存在该形状", "查找失败", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

    }


}
