package jpaint;

import jpaint.draw.area.DrawArea;
import jpaint.file.io.FileIO;
import jpaint.menu.bar.MenuBar;
import jpaint.shape.manager.ShapeManager;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;


public class JPaint extends JFrame implements Serializable {

    protected String fileName = "�ޱ���";
    protected String fileType = null;
    protected String filePath = null;
    protected final FileIO fileIO = new FileIO();
    protected final MenuBar menuBar = new MenuBar();
    protected final DrawArea drawArea = new DrawArea();
    protected final ShapeManager shapeManager = new ShapeManager();

    public JPaint() {
        JPanel centerPanel = new JPanel();
        centerPanel.setLayout(new BorderLayout());

        centerPanel.add(new JScrollPane(drawArea), BorderLayout.CENTER);
        centerPanel.add(shapeManager, BorderLayout.EAST);

        this.setTitle(fileName + " - ��ͼ");
        this.setLayout(new BorderLayout());

        this.add(menuBar, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
        
        this.setLocation(150, 100);
        this.pack();
        this.setVisible(true);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("image//JPaint.png"));

        new Listeners(this, fileIO, menuBar, drawArea, shapeManager);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
